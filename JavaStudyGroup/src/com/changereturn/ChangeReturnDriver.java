package com.changereturn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class ChangeReturnDriver {
	
	//2 dimensional array creating and printing, formatting decimal for output, 
	//parse Integer and Double from String, "|" & OR logical operator, boolean, while loop, 
	//reading from eclipse console
	
	static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

	public static void main(String[] args) {

		String[][] items = {{"Snickers","1.00"} , {"Doritos","1.35"} , {"Pepsi","1.80"}};
		DecimalFormat df = new DecimalFormat("##0.00");
		double cost = 0.00;
		double money = 0.00;
		
		//print items and get selection
		String choice = printVendingMachine(items);

		//verify selection is valid
		boolean keepLooping = true;
		while (keepLooping) {
			if (choice.equals("99"))
				System.exit(0);
			else if ((Integer.parseInt(choice) < 1 | Integer.parseInt(choice) > items.length)) {
				System.out.println("\n*** Invalid entry,please select again! ***\n");
				choice = printVendingMachine(items);
			} else {
				keepLooping = false;
				cost = Double.parseDouble(items[Integer.parseInt(choice) - 1][1]);
			}
		} //close while
		
		//get money deposited and verify it is enough to cover cost
		System.out.print("Enter amount of money deposited: ");
		String moneyStr = "";
		try {
			moneyStr = bufferedReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		money = Double.parseDouble(moneyStr);
		System.out.println("\nYou deposited $" + df.format(money) + " money and cost is $" + df.format(cost));
		
		if(cost > money){
			System.out.println("\n*** ERROR-you didn't deposit enough money! ***\n");
			choice = printVendingMachine(items);
		}
		
		//figure out quarters, dimes, nickels, and pennies
		double change = round2(money - cost);
		int quarters = 0, dimes = 0, nickels = 0, pennies = 0;
		
		while (change >= .25){
			quarters++;
			change -= .25;
		}
		while (change >= .10){
			dimes++;
			change -= .10;
		}
		while (change >= .05){
			nickels++;
			change -= .05;
		}
		while (change >= .01){
			pennies++;
			change -= .01;
		}
		
		System.out.println(quarters + " quarters, " + dimes + " dimes, " + nickels + " nickels, " + pennies + " pennies ");
		
	}//close main
	
	
	//prints all the items and reads in the selection
	public static String printVendingMachine(String[][] items) {

		System.out.println("   Item         Price");
		System.out.println("---------------------");
		String itemLine = "";

			for (int row = 0; row < items.length; row++) {
				itemLine += (row + 1) + ": ";
				for (int col = 0; col < items[row].length; col++) {
					itemLine += items[row][col] + "\t";
				}
				System.out.println(itemLine);
				itemLine = "";
			} // close for
			System.out.println("99: Exit");

			System.out.print("\nType the number of the item you want: ");
			String str = "";
			try {
				str = bufferedReader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;

	}// close method
	
	
	//rounds a number up to 2 decimal places
	public static Double round2(Double val){
		return new BigDecimal(val.toString()).setScale(2,RoundingMode.HALF_UP).doubleValue();
	}

}//close class
