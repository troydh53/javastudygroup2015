package com.demos;



public class ReferenceDriver {

	public static void main(String[] args) {

		int x = 0;

		String str = "Me";
		
		
		System.out.println("Before method X = " + x + " and str = " + str);
		
		changeVals(x, str);
		
		System.out.println("After method X = " + x + " and str = " + str);
		
		
	}//close main

	
	static void changeVals(int x, String str){
		
		x = 5;
		str = "You";
		
		System.out.println("Inside method X = " + x + " and str = " + str);
				
	}
	
	

	

	
	
}//close class
