package com.demos;

public class Dog extends Animal {
	
	@Override	
	void makeNoise(){
		
		System.out.println("I say Bark!");
	}
	
	
	void myDescription(int x){
		System.out.println("I have " + x + " legs!");
	}
	
	
	void myDescription(int x, String color){
		System.out.println("I have " + x + " legs and am colord " + color + "!");
	}

}
