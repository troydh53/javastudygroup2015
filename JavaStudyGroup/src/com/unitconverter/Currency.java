package com.unitconverter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Currency implements Measurement {

	private static Scanner sc = new Scanner(System.in);

	// Display Units of Measurement and read in selection
	@Override
	public void displayUnits() {
		int convUnit = 0;
		boolean loopAgain = true;

		do {
			System.out.println("\nSelect the Unit conversion:");
			System.out.println("1. US Dollar to Canadian Dollar");
			System.out.println("2. Canadian Dollar to US Dollar");
			System.out.println("99. Exit");
			convUnit = sc.nextInt();

			switch (convUnit) {
			case 1:
				loopAgain = false;
				break;
			case 2:
				loopAgain = false;
				break;
			case 99:
				System.exit(0);
				break;
			default:
				System.out.println("\nERROR-Invalid Entry, try again.\n");
				break;
			}// close switch

		} while (loopAgain);

		calcResults(convUnit);
	}

	
	public void calcResults(int selection) {
		double canDollar = 0.0;
		double usDollar = 0.0;

		DecimalFormat df = new DecimalFormat("##0.00");

		if (selection == 1) {
			System.out.println("Enter US Dollar to convert: ");
			usDollar = sc.nextDouble();
			canDollar = usDollar * 1.34;
			System.out.println(
					"$" + df.format(usDollar) + " US Dollars are $" + df.format(canDollar) + " Canadian Dollars");
		} else if (selection == 2) {
			System.out.println("Enter Canadian Dollar to convert: ");
			canDollar = sc.nextDouble();
			usDollar = canDollar * .75;
			System.out.println(
					"$" + df.format(canDollar) + " Canadian Dollars are $" + df.format(usDollar) + " US Dollars");
		}

	}// close calcResults

}
