package com.unitconverter;

import java.util.Scanner;

public class ConverterDriver {

	private static int convType = 0;
	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		displayMenu();

	}// close main method1

	
	// Display menu and read selection
	private static void displayMenu() {

		Measurement convTypeO = null;
		boolean loopAgain = true;

		do {
			System.out.println("Select Type of conversion:");
			System.out.println("1. Tempurature");
			System.out.println("2. Volume");
			System.out.println("3. Currency");
			System.out.println("99: Exit");

			convType = sc.nextInt();

			switch (convType) {
			case 1:
				convTypeO = new Temperature();
				loopAgain = false;
				break;
			case 2:
				convTypeO = new Volume();
				loopAgain = false;
				break;
			case 3:
				convTypeO = new Currency();
				loopAgain = false;
				break;
			case 99:
				System.out.println("Exiting Program.");
				System.exit(0);
				break;
			default:
				System.out.println("\nERROR-Invalid Entry, try again.\n\n");
				break;
			}// close switch

		} while (loopAgain);

		convTypeO.displayUnits(); // Display Units of different types of
									// Measurements

	}// close displayMenu

}// close Class
