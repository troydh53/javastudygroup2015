package com.unitconverter;

public interface Measurement {

	public void displayUnits();

	public void calcResults(int selection);

}