package com.unitconverter;

import java.util.Scanner;

public class Temperature implements Measurement {

	private static Scanner sc = new Scanner(System.in);

	// Display Units of Measurement and read in selection
	@Override
	public void displayUnits() {
		int convUnit = 0;
		boolean loopAgain = true;

		do {
			System.out.println("\nSelect the Unit conversion:");
			System.out.println("1. Fahrenheit to Celsius");
			System.out.println("2. Celsius to Fahrenheit");
			System.out.println("99. Exit");
			convUnit = sc.nextInt();

			switch (convUnit) {
			case 1:
				loopAgain = false;
				break;
			case 2:
				loopAgain = false;
				break;
			case 99:
				System.exit(0);
				break;
			default:
				System.out.println("\nERROR-Invalid Entry, try again.\n");
				break;
			}// close switch

		} while (loopAgain);

		calcResults(convUnit);

	}
	public void calcResults(int selection) {
		double cTemp = 0.0;
		double fTemp = 0.0;
		if (selection == 1) {
			System.out.println("Enter Fahrenheit temp to convert: ");
			fTemp = sc.nextDouble();
			cTemp = (fTemp - 32) * .5556;
			System.out.println(fTemp + " degrees Fahrenheit is " + cTemp + " degrees Celsius");
		} else if (selection == 2) {
			System.out.println("Enter Celsius temp to convert: ");
			cTemp = sc.nextDouble();
			fTemp = cTemp * 1.8 + 32;
			System.out.println(cTemp + " degrees Celsius is " + fTemp + " degrees Fahrenheit");
		}

	}// close calcResults

}
