package com.unitconverter;

import java.util.Scanner;

public class Volume implements Measurement {

	private static Scanner sc = new Scanner(System.in);

	// Display Units of Measurement and read in selection
	@Override
	public void displayUnits() {
		int convUnit = 0;
		boolean loopAgain = true;

		do {
			System.out.println("\nSelect the Unit conversion:");
			System.out.println("1. Oz to Cup");
			System.out.println("2. Cup to Pint");
			System.out.println("99. Exit");
			convUnit = sc.nextInt();

			switch (convUnit) {
			case 1:
				loopAgain = false;
				break;
			case 2:
				loopAgain = false;
				break;
			case 99:
				System.exit(0);
				break;
			default:
				System.out.println("\nERROR-Invalid Entry, try again.\n");
				break;
			}// close switch

		} while (loopAgain);

		calcResults(convUnit);
	}

	public void calcResults(int selection) {
		double ozAmt = 0.0;
		double cupAmt = 0.0;
		double pintAmt = 0.0;

		if (selection == 1) {
			System.out.println("Enter liquid ounces to convert: ");
			ozAmt = sc.nextDouble();
			cupAmt = ozAmt * 0.125;
			System.out.println(ozAmt + " ounces are " + cupAmt + " Cups");
		} else if (selection == 2) {
			System.out.println("Enter cups to convert: ");
			cupAmt = sc.nextDouble();
			pintAmt = cupAmt * 0.5;
			System.out.println(cupAmt + " Cups are " + pintAmt + " Pints");
		}

	}// close calcResults

}
