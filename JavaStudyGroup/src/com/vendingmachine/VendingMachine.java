package com.vendingmachine;

import java.util.Map;
import java.util.Map.Entry;

public class VendingMachine {
	
	private Map<Integer, VendingMachineItem> items;
	
	//constructor
	public VendingMachine(Map<Integer, VendingMachineItem> items){
		this.items=items;
	}
	
	//Prints the menu with columns formatted nicely
	public void printVendingMachine() {
		
		GenericFormat outStr = new GenericFormat();
		
		System.out.println("Item#   Name       Price     Qty");
		System.out.println("-------------------------------------");

		for( Entry<Integer, VendingMachineItem> entry : items.entrySet()) {
			String str = "";
			str += outStr.formatObj(entry.getKey(), 8, true);
			str += outStr.formatObj(entry.getValue().getName(), 12, true);
			str += "$" + outStr.formatObj(entry.getValue().getPrice(), 9, true);
			str += outStr.formatObj(entry.getValue().getQty(), 5, true);
			System.out.println(str);
		}
		
		System.out.println("\nEnter money deposited or -1 to Exit: ");
	}
	
	//confirms if chose a valid item
	public boolean validSelection(int key) {
		if ( items.containsKey(key) ){
			return true;
		} else return false;
		
	}
	//gets item if it exists
	public VendingMachineItem getItem(int key){
			return items.get(key);
	}
	
	//updates qty of items taken
	public boolean updateItem(int key){
		try{
			VendingMachineItem item = this.getItem(key);
			if ( item.getQty() > 0 ){
				item.takeItem();
				return true;
			}
		} catch ( IllegalArgumentException e){
			//false
		}
		return false;
	}
	
	//calculates change from money entered
	public String calculateChange(int key, double money){
		try{
			VendingMachineItem item = this.getItem(key);
			double cost = item.getPrice();
			if ( money >= cost ){
				double remainder = money - cost;
				int quarters = 0;
				int dimes = 0;
				int nickels = 0;
				int pennies = 0;
				while( remainder >= 0.25 ){
					quarters++;
					remainder -= .25;
				}
				while( remainder >= 0.10){
					dimes++;
					remainder -= .10;
				}
				while( remainder >= 0.05){
					nickels++;
					remainder -= .05;
				}
				while( remainder >= 0.01){
					pennies++;
					remainder -= .01;
				}
				return "\n-- You chose " + item.getName() + ", Your change is "+quarters+" quarters, "+dimes+" dimes, " + nickels +" nickels, " + pennies +" pennies.\n";
			}
			else{
				return "-- You did not put enough money into the machine!";
			}
		} catch( IllegalArgumentException e ){
			return "-- This machine is out of that item!";
		}
	}
	
	//prints vending machine
	@Override
	public String toString(){
		String str = "";
		
		for( Entry<Integer,VendingMachineItem> entry : items.entrySet() ){
			str += entry.getKey() + " : " + entry.getValue().toString() + "\n";
		}
		
		return str;
	}

}