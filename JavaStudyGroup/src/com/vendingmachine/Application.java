package com.vendingmachine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Application {
	
	private static VendingMachine machine = new VendingMachine(generateMachine());
	private static Scanner sc = new Scanner(System.in);

	
	public static void main(String[] args) {

		String strInput = "";
		int choice = 0;
		double moneyInput = 0.0;
		boolean keepLooping = true;
		
		do {
			//Print Vending Machine, check if selected to Exit program
			machine.printVendingMachine();
			strInput = sc.nextLine();
			checkForExit(strInput);
			
			//Validate money entered was numbers not letters.
			try{
			moneyInput = Double.parseDouble(strInput);
			} catch (NumberFormatException e){
				System.out.println("*** ERROR - Enter money in decimal format, enter again!\n");
				continue;
			}
			
			//Read item selected a confirm a valid selection
			System.out.println("Select your item or -1 to Exit: ");
			strInput = sc.nextLine();
			checkForExit(strInput);

			try{
			choice = Integer.parseInt(strInput);
			} catch (NumberFormatException e){
				System.out.println("*** ERROR - Your selection must be a number, Please start over!\n");
				continue;
			}
			if (!machine.validSelection(choice)) {
				System.out.println("*** ERROR - Invalid selection, Please start over!\n");
				continue;
			}
			
			//Valid item has been selected, print change and exit loop
			VendingMachineItem itemChose = machine.getItem(choice);
			boolean itemAvailable = machine.updateItem(choice);
			if(!itemAvailable){
				System.out.println("-- Sorry, out of that item, Please start over!\n");
				continue;
			}
			
			//keepLooping = false;
			char[] vending = {'V', 'E', 'N', 'D', 'I', 'N', 'G', '*', '*', '*', '*', '*', '*', '*', '*' };
			
			for(int i = 0; i < vending.length; i++){
				System.out.print(vending[i]);
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
						
			System.out.println(machine.calculateChange(choice, moneyInput));

		} while(keepLooping);
		

	}

	//creates VendingMachine and puts Items into it.
	private static Map<Integer,VendingMachineItem> generateMachine(){
		Map<Integer,VendingMachineItem> machine = new TreeMap<>();
		
		File file = new File("C:/git/javastudygroup2015/JavaStudyGroup/bin/com/vendingmachine/items.txt");
		String line;
		int selection = 10;
		
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

		//While there's more to read from the file
		while( br.ready() ){
			selection++;
			line = br.readLine(); //Read one line in
			String[] attributes = line.split(","); //We parse on commas in this example
			machine.put(new Integer(selection), new VendingMachineItem(attributes[0].trim(), Double.parseDouble(attributes[1].trim()), Integer.parseInt(attributes[2].trim()))); //Add values to our custom object

		}//close while
		
		} catch (IOException e) {
			e.printStackTrace();

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		return machine;
	}
	
	private static void checkForExit(String input){
		if (input.equals("-1")) {
			System.out.println("Exiting Program!");
			System.exit(0);
		}
		
	}
}