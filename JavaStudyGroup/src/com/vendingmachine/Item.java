package com.vendingmachine;

public class Item {
	
	private final String name;
	private final double price;
	
	Item(String name, double price){
		this.name = name;
		this.price = price;
	}
	
	public String getName() {
		return name;
	}
	public double getPrice() {
		return price;
	}
	
	

}
