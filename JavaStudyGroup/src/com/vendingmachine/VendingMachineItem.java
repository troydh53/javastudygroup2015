package com.vendingmachine;

public class VendingMachineItem {
	
	private String name;
	private double price;
	private int qty;

	//Constructor
	public VendingMachineItem(String name, double price, int qty) {
		this.name = name;
		this.price = price;
		this.qty = qty;
	}

	//--Getters and Setters --
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	//retuced qty of item taken
	public void takeItem() {
		qty--;
	}

	public int getQty() {
		return qty;
	}
	
	
	@Override
	public String toString(){
		return "{ " + name + ", $" + price + ", " + qty + " }";
	}
}