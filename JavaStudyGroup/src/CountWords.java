import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CountWords {

	public static void main(String[] args) {

		//comment, eclipse console read in, output, String, String equals, 
		//if-else, String array, split method, try catch, new line escape char. 
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String str = "";
		System.out.print("Please enter some text:\n");
		try {
			str = bufferedReader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Console console = System.console();
		//String str = console.readLine("Please enter some text: ");
		if(str.equals("")){
			System.out.println("You didn't enter any words!");
		} else {
		String[] words = str.split(" ");
		System.out.println("Total words entered are: " + words.length);
		}
		
		
	}

}